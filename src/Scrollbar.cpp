/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <SFTK/Scrollbar.hpp>

using namespace sftk;

Scrollbar::Scrollbar(float s, bool v)
	: Slider(v,0), scale(s) {}

Scrollbar::~Scrollbar() {}

void Scrollbar::draw(const Graphics& graphics, float z = 0) const {
	// TODO
}

float Scrollbar::getScale() const {
	return scale;
}

float Scrollbar::getScroll() const {
	return (scale * value);
}

bool Scrollbar::isScrollbar() const {
	return true;
}

void Scrollbar::setScale(float s) {
	scale = s;
}

void Scrollbar::setScroll(float scroll) {
	setValue(scroll / scale);
}
