/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <iostream>
#include <SFTK/Dropdown.hpp>

using namespace sf;
using namespace sftk;
using namespace std;

void Dropdown::fixChild(Component* child) {
	Container::fixChild(child);

	FloatRect b(child->getBounds());
	b.left = ((bounds.width - b.width) / (float)2);
	b.top = ((bounds.height - b.height) / (float)2);
	child->setBounds(b);
}

void Dropdown::getDownSize() {
	downSize.y = bounds.height;
	for (Iterator c = children.begin(); c != children.begin(); ++c)
		downSize.y += (*c)->bounds.height;
}

void Dropdown::onBoundsChanged(float dl, float dt, float dw, float dh) {
	Container::onBoundsChanged(dl,dt,dw,dh);
	downSize.x = bounds.width;
	getDownSize();
}

bool Dropdown::onJoystickButtonPressed(unsigned id, unsigned button, float x, float y) {
	if (!isEnabled()) return false;

	if (downShow) {
		x -= bounds.left;
		y -= (bounds.top - bounds.height);

		for (Iterator c = children.begin(); c != children.end(); ++c) {
			Component* child = (*c);
			if (child->contains(x,y)) {
				if (child->onJoystickButtonPressed(id,button,x,y))
					return true;
			}
			y -= child->bounds.height;
		}
	}

	return false;
}

bool Dropdown::onJoystickButtonReleased(unsigned id, unsigned button, float x, float y) {
	if (!isEnabled()) return false;

	if (downShow && (!Container::contains(x,y))) {
		x -= bounds.left;
		y -= (bounds.top - bounds.height);

		for (Iterator c = children.begin(); c != children.end(); ++c) {
			Component* child = (*c);
			if (child->contains(x,y)) {
				if (child->onJoystickButtonReleased(id,button,x,y))
					return true;
			}
			y -= child->bounds.height;
		}
	}

	return false;
}

bool Dropdown::onJoystickMoved(unsigned id, float x, float y) {
	if (!isEnabled()) return false;

	if (downShow && (!Container::contains(x,y))) {
		x -= bounds.left;
		y -= (bounds.top - bounds.height);

		for (Iterator c = children.begin(); c != children.end(); ++c) {
			Component* child = (*c);
			if (child->contains(x,y)) {
				setHover(child);
				if (child->onJoystickMoved(id,x,y))
					return true;
			}
			y -= child->bounds.height;
		}
	}

	return false;
}

bool Dropdown::onMouseButtonPressed(Mouse::Button button, float x, float y) {
	if (!isEnabled()) return false;

	if (downShow) {
		if (Container::contains(x,y) && (button == Mouse::Button::Left)) {
			downShow = false;
			return true;
		}

		x -= bounds.left;
		y -= (bounds.top - bounds.height);

		for (Iterator c = children.begin(); c != children.end(); ++c) {
			Component* child = (*c);
			if (child->contains(x,y)) {
				if (child->onMouseButtonPressed(button,x,y))
					return true;
			}
			y -= child->bounds.height;
		}
	} else if (button == Mouse::Button::Left) {
		downShow = true;
		return true;
	}

	return false;
}

bool Dropdown::onMouseButtonReleased(sf::Mouse::Button button, float x, float y) {
	if (!isEnabled()) return false;

	if (downShow && (!Container::contains(x,y))) {
		x -= bounds.left;
		y -= (bounds.top - bounds.height);

		for (Iterator c = children.begin(); c != children.end(); ++c) {
			Component* child = (*c);
			if (child->contains(x,y)) {
				if (child->onMouseButtonReleased(button,x,y))
					return true;
			}
			y -= child->bounds.height;
		}
	}

	return false;
}

bool Dropdown::onMouseMoved(float x, float y) {
	if (!isEnabled()) return false;

	if (downShow && (!Container::contains(x,y))) {
		x -= bounds.left;
		y -= (bounds.top - bounds.height);

		for (Iterator c = children.begin(); c != children.end(); ++c) {
			Component* child = (*c);
			if (child->contains(x,y)) {
				setHover(child);
				if (child->onMouseMoved(x,y))
					return true;
			}
			y -= child->bounds.height;
		}
	}

	return false;
}

bool Dropdown::onTouchBegan(unsigned finger, float x, float y) {
	if (!isEnabled()) return false;

	if (downShow) {
		if (Container::contains(x,y)) {
			downShow = false;
			return true;
		}

		x -= bounds.left;
		y -= (bounds.top - bounds.height);

		for (Iterator c = children.begin(); c != children.end(); ++c) {
			Component* child = (*c);
			if (child->contains(x,y)) {
				if (child->onTouchBegan(finger,x,y))
					return true;
			}
			y -= child->bounds.height;
		}
	} else {
		downShow = true;
		return true;
	}

	return false;
}

bool Dropdown::onTouchEnded(unsigned finger, float x, float y) {
	if (!isEnabled()) return false;

	if (downShow && (!Container::contains(x,y))) {
		x -= bounds.left;
		y -= (bounds.top - bounds.height);

		for (Iterator c = children.begin(); c != children.end(); ++c) {
			Component* child = (*c);
			if (child->contains(x,y)) {
				if (child->onTouchEnded(finger,x,y))
					return true;
			}
			y -= child->bounds.height;
		}
	}

	return false;
}

bool Dropdown::onTouchMoved(unsigned finger, float x, float y) {
	if (!isEnabled()) return false;

	if (downShow && (!Container::contains(x,y))) {
		x -= bounds.left;
		y -= (bounds.top - bounds.height);

		for (Iterator c = children.begin(); c != children.end(); ++c) {
			Component* child = (*c);
			if (child->contains(x,y)) {
				if (child->onTouchMoved(finger,x,y))
					return true;
			}
			y -= child->bounds.height;
		}
	}

	return false;
}

void Dropdown::onVisibilityChanged() {
	if (!isVisible())
		downShow = false;
}

Dropdown::Dropdown() : Container(),
	downSize(bounds.width,bounds.height), downShow(false) {}

Dropdown::~Dropdown() {}

void Dropdown::addComponent(Component* component) {
	if (component->isContainer())
		cerr << "SFTK: cannot add container to dropdown" << endl;
	else {
		Container::addComponent(component);
		getDownSize();
	}
}

bool Dropdown::contains(float x, float y) const {
	if (Component::contains(x,y))
		return true;
	else if (downShow) {
		x -= bounds.left;
		y -= bounds.top;

		return ((x >= 0) && (x <= downSize.x) &&
				(y >= 0) && (y <= downSize.y));
	} else
		return false;
}

size_t Dropdown::getCount() const {
	return children.size();
}

void Dropdown::removeComponent(Component* component) {
	Container::removeComponent(component);
	getDownSize();
}
