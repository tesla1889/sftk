/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <SFTK/Clipboard.hpp>
#include <SFTK/TextComponent.hpp>

using namespace sf;
using namespace sftk;
using namespace std;

bool TextComponent::onKeyPressed(const Event::KeyEvent& event) {
	if (!(isEnabled() && hasFocus())) return false;

	switch (event.code) {
	case Keyboard::C:
		if (event.system && (mode & TEXTMODE_SELECT))
			Clipboard::set(content,prefixEnd,suffixStart);
		else
			return false;
		break;
	case Keyboard::Down:
		if (event.shift) {
			if (prefixEnd < 0)
				prefixEnd = cursor;
			suffixStart = content.length();
			mode |= TEXTMODE_SELECT;
		}
		cursor = content.length();
		break;
	case Keyboard::Insert:
		mode ^= TEXTMODE_INSERT;
		break;
	case Keyboard::Left:
		if (event.shift) {
			prefixEnd = (cursor - 1);
			if (suffixStart < 0)
				suffixStart = cursor;
			mode |= TEXTMODE_SELECT;
		}
		--cursor;
		break;
	case Keyboard::Right:
		if (event.shift) {
			if (prefixEnd < 0)
				prefixEnd = cursor;
			suffixStart = (cursor + 1);
			mode |= TEXTMODE_SELECT;
		}
		++cursor;
		break;
	case Keyboard::Up:
		if (event.shift) {
			prefixEnd = 0;
			if (suffixStart < 0)
				suffixStart = cursor;
			mode != TEXTMODE_SELECT;
		}
		cursor = 0;
		break;
	case Keyboard::V:
		if (event.system && (mode & TEXTMODE_SELECT))
			setSelectedText(Clipboard::get());
		else
			return false;
		break;
	default:
		return false;
	}

	return true;
}

bool TextComponent::onTextEntered(Uint32 unicode) {
	if (!isEnabled()) return false;

	content.insert(cursor,1,(char)unicode);
	if ((mode & TEXTMODE_INSERT) == 0)
		++cursor;

	return true;
}

TextComponent::TextComponent(const char* text)
	: Component(), selectionBackground(foreground), selectionForeground(background),
	  content(text), cursor(0), prefixEnd(-1), suffixStart(-1), mode(0) {}

TextComponent::TextComponent(const string& text)
	: Component(), selectionBackground(foreground), selectionForeground(background),
	  content(text), cursor(0), prefixEnd(-1), suffixStart(-1), mode(0) {}

TextComponent::~TextComponent() {}

size_t TextComponent::getCursor() const {
	return cursor;
}

size_t TextComponent::getLength() const {
	return content.length();
}

const string& TextComponent::getSelectedText() const {
	if (mode & TEXTMODE_SELECT)
		return content.substr(prefixEnd,suffixStart);
	else
		return string();
}

const Color& TextComponent::getSelectionBackground() const {
	return selectionBackground;
}

const Color& TextComponent::getSelectionForeground() const {
	return selectionForeground;
}

const string& TextComponent::getText() const {
	return content;
}

void TextComponent::setCursor(size_t c) {
	size_t len = content.length();
	content = ((c > len) ? len : c);
}

void TextComponent::setSelectedText(const char* text) {
	setSelectedText(string(text));
}

void TextComponent::setSelectedText(const string& text) {
	if (mode & TEXTMODE_SELECT) {
		content.replace(prefixEnd,suffixStart,text);
		cursor = (prefixEnd + text.length());
		prefixEnd = (-1);
		suffixStart = (-1);
		mode ^= TEXTMODE_SELECT;
	}
}

void TextComponent::setSelectionBackground(Uint8 r, Uint8 g, Uint8 b, Uint8 a) {
	selectionBackground.r = r;
	selectionBackground.g = g;
	selectionBackground.b = b;
	selectionBackground.a = a;
}

void TextComponent::setSelectionBackground(const Color& b) {
	selectionBackground.r = b.r;
	selectionBackground.g = b.g;
	selectionBackground.b = b.b;
	selectionBackground.a = b.a;
}

void TextComponent::setSelectionForeground(Uint8 r, Uint8 g, Uint8 b, Uint8 a) {
	selectionForeground.r = r;
	selectionForeground.g = g;
	selectionForeground.b = b;
	selectionForeground.a = a;
}

void TextComponent::setSelectionForeground(const Color& f) {
	selectionForeground.r = f.r;
	selectionForeground.g = f.g;
	selectionForeground.b = f.b;
	selectionForeground.a = f.a;
}

void TextComponent::setText(const char* text) {
	setText(string(text));
}

void TextComponent::setText(const string& text) {
	content = text;
	cursor = content.length();
	prefixEnd = (-1);
	suffixStart = (-1);
	mode &= TEXTMODE_INSERT;
}
