/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <math.h>
#include <SFTK/OptionList.hpp>

using namespace sf;
using namespace sftk;

void OptionList::onJoystickButtonPressed(unsigned id, unsigned button, float x, float y) {
	// TODO
}

bool OptionList::onKeyPressed(const Event::KeyEvent& event, float x, float y) {
	if (!enabled()) return false;

	int count = (int)children.size();

	switch (event.code) {
	case Keyboard::Down:
		if (selection < (count - 1))
			++selection;
		break;
	case Keyboard::Return:
		downShow = (!downShow);
		break;
	case Keyboard::Up:
		if (selection > (allowEmpty ? (-1) : 0))
			--selection;
		break;
	default:
		return false;
	}

	return true;
}

bool OptionList::onMouseButtonPressed(Mouse::Button button, float x, float y) {
	// TODO
	return true;
}

bool OptionList::onMouseScrolled(bool vertical, float delta, float x, float y) {
	if (!enabled()) return false;

	int ticks = (int)round((double)delta);
	if (ticks == 0) return true;

	int select = (selection - ticks);
	if (select < selection) {
		int min = (allowEmpty ? (-1) : 0);
		if (select < min)
			select = min;
		selection = select;
	} else {
		int max = ((int)children.size() - 1);
		if (select > max)
			select = max;
		selection = select;
	}

	return true;
}

bool OptionList::onTouchBegan(unsigned finger, float x, float y) {
	// TODO
	return true;
}

OptionList::OptionList(bool allow)
	: Dropdown(), selection(-1), allowEmpty(allow) {}

OptionList::~OptionList() {}

void OptionList::addComponent(Component* component) {
	Dropdown::addComponent(component);

	if ((!allowEmpty) && (selection < 0))
		selection = 0;
}

bool OptionList::allowsEmpty() const {
	return allowEmpty;
}

void OptionList::draw(const Graphics& graphics, float z) const {
	// TODO
}

int OptionList::getSelectedIndex() const {
	return selection;
}

Component* OptionList::getSelectedOption() {
	if (selection < 0)
		return NULL;

	Iterator c = children.begin();
	int n;
	for (n = 0; (c != children.end()) && (n < selection); ++n) ++c;
	if ((c != children.end()) || (n == selection))
		return (*c);

	return NULL;
}

const Component* OptionList::getSelectedOption() const {
	if (selection < 0)
		return NULL;

	ConstIterator c = children.begin();
	int n;
	for (n = 0; (c != children.end()) && (n < selection); ++n) ++c;
	if ((c != children.end()) || (n == selection))
		return (*c);

	return NULL;
}

void OptionList::removeComponent(Component* component) {
	Component* select = getSelectedOption();
	if ((select != NULL) && (select == component)) {
		if (selection >= 0)
			--selection;
		if ((selection < 0) && (!allowEmpty) && (getCount() > 0))
			selection = 0;
	}

	Dropdown::removeComponent(component);
}

void OptionList::setAllowsEmpty(bool allow) {
	if ((!(allowEmpty = allow)) && (selection < 0) && (getCount() > 0))
		selection = 0;
}

void OptionList::setSelectedIndex(int index) {
	int count = (int)getCount();
	if (count == 0)
		selection = (-1);
	else if (index < 0) {
		if (allowEmpty)
			selection = (-1);
	} else if (index < count)
		selection = index;
}

void OptionList::setSelectedOption(Component* component) {
	Iterator c = children.begin();
	for (int n = 0; c != children.end(); ++n) {
		Component* child = (*c);
		if (child == component) {
			selection = n;
			break;
		} else
			++c;
	}
}
