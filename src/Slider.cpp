/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <math.h>
#include <SFTK/Slider.hpp>

using namespace sf;
using namespace sftk;

void Slider::onCursorSelect(float x, float y) {
	float select = (vertical ? y : x);
	float max = (vertical ? bounds.height : bounds.width);
	value = (select / max);
	perform();
}

bool Slider::onKeyPressed(const Event::KeyEvent& event) {
	if (!isEnabled()) return false;
	else if (vertical) {
		switch (event.code) {
		case Keyboard::Down:
			decrement();
			break;
		case Keyboard::Up:
			increment();
			break;
		default:
			return false;
		}
	} else {
		switch (event.code) {
		case Keyboard::Left:
			decrement();
			break;
		case Keyboard::Right:
			increment();
			break;
		default:
			return false;
		}
	}

	return true;
}

bool Slider::onMouseButtonPressed(Mouse::Button button, float x, float y) {
	if (isEnabled() && (button == Mouse::Button::Left)) {
		onCursorSelect(x,y);
		return true;
	}

	return false;
}

bool Slider::onMouseMoved(float x, float y) {
	if (isEnabled() && Mouse::isButtonPressed(Mouse::Button::Left)) {
		onCursorSelect(x,y);
		return true;
	}

	return false;
}

bool Slider::onMouseScrolled(bool v, float delta, float x, float y) {
	if (isEnabled() && (vertical == v)) {
		setValue(value - (delta * step));
		return true;
	}

	return false;
}

bool Slider::onTouchBegan(unsigned finger, float x, float y) {
	if (isEnabled()) {
		onCursorSelect(x,y);
		return true;
	}

	return false;
}

bool Slider::onTouchMoved(unsigned finger, float x, float y) {
	if (isEnabled()) {
		onCursorSelect(x,y);
		return true;
	}

	return false;
}

Slider::Slider(bool vert, float val, float s)
: ActionComponent(), step(s), vertical(vert) {
	setValue(val);
}

void Slider::decrement() {
	setValue(value - step);
}

void Slider::draw(const Graphics& graphics, float z = 0) const {
	// TODO
}

void Slider::increment() {
	setValue(value + step);
}

bool Slider::isVertical() const {
	return vertical;
}

float Slider::getStep() const {
	return step;
}

float Slider::getValue() const {
	return value;
}

void Slider::setStep(float s) {
	step = s;
}

void Slider::setValue(float v) {
	if (v < 0) v = 0;
	else if (v > 1) v = 1;
	value = v;
	perform();
}

void Slider::setVertical(bool v) {
	vertical = v;
}
