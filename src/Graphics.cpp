/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <SFML/OpenGl.hpp>
#include <SFTK/Graphics.hpp>

using namespace sf;
using namespace sftk;
using namespace std;

void Graphics::fix(float& left, float& top, float& width, float& height) const {
	if (left < bounds.left)
		left = bounds.left;

	if (top < bounds.top)
		top = bounds.top;

	float dx = ((left + width) - bounds.width);
	if (dx > 0)
		width -= dx;

	float dy = ((top + height) - bounds.height);
	if (dy > 0)
		height -= dy;
}

union TargetDeconst {
	const RenderWindow* in;
	RenderWindow* out;
};

Graphics::Graphics(const Graphics& graphics)
: bounds(graphics.bounds) {
	TargetDeconst w;
	w.in = graphics.target;
	target = w.out;
}

Graphics::Graphics(const RenderWindow* window)
: bounds(0,0,1,1) {
	TargetDeconst w;
	w.in = window;
	target = w.out;
}

void Graphics::changeBounds(float dl, float dt, float dw, float dh) {
	bounds.left += dl;
	bounds.top += dt;
	bounds.width += dw;
	bounds.height += dh;
}

void Graphics::drawRect(const FloatRect& rect, float z) const {
	drawRect(rect.left,rect.top,rect.width,rect.height,z);
}

void Graphics::drawRect(float left, float top, float width, float height, float z) const {
	fix(left,top,width,height);

	if ((width <= 0) || (height <= 0)) return;

	float bottom = (top + height);
	float right = (left + width);

	lock_guard<mutex>(_mutex);
	target->setActive(true);
	glBegin(GL_LINES);
	glVertex3f(left,top,z);
	glVertex3f(right,top,z);
	glVertex3f(right,top,z);
	glVertex3f(right,bottom,z);
	glVertex3f(right,bottom,z);
	glVertex3f(left,bottom,z);
	glVertex3f(left,bottom,z);
	glVertex3f(left,top,z);
	glEnd();
	target->setActive(false);
}

void Graphics::drawString(const String& s, float left, float top) const {
	Text text(s);
	text.setOrigin((bounds.left + left),(bounds.top + top));

	lock_guard<mutex>(_mutex);
	target->setActive(true);
	text.draw(*target,RenderStates::Default);
	target->setActive(false);
}

void Graphics::fillRect(const FloatRect& rect, float z) const {
	fillRect(rect.left,rect.top,rect.width,rect.height,z);
}

void Graphics::fillRect(float left, float top, float width, float height, float z) const {
	fix(left,top,width,height);

	if ((width <= 0) || (height <= 0)) return;

	float bottom = (top + height);
	float right = (left + width);

	lock_guard<mutex>(_mutex);
	target->setActive(true);
	glBegin(GL_QUADS);
	glVertex3f(left,top,z);
	glVertex3f(right,top,z);
	glVertex3f(right,bottom,z);
	glVertex3f(left,bottom,z);
	glEnd();
	target->setActive(false);
}

const FloatRect& Graphics::getBounds() const {
	return bounds;
}

const RenderWindow* Graphics::getTarget() const {
	return target;
}

void Graphics::setBounds(const FloatRect& b) {
	bounds.left = b.left;
	bounds.top = b.top;
	bounds.width = b.width;
	bounds.height = b.height;
}

void Graphics::setBounds(float left, float top, float width, float height) {
	bounds.left = left;
	bounds.top = top;
	bounds.width = width;
	bounds.height = height;
}

void Graphics::setColor(const Color& c) const {
	setColor(c.r,c.g,c.b,c.a);
}

void Graphics::setColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a) const {
	lock_guard<mutex>(_mutex);
	target->setActive(true);
	glColor4b(r,g,b,a);
	target->setActive(false);
}
