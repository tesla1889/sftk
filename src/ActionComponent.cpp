/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <SFTK/ActionComponent.hpp>

using namespace sftk;

ActionHandler::~ActionHandler() {}

void ActionComponent::perform() {
	if (handler != NULL)
		handler->onAction(action,this);
}

ActionComponent::ActionComponent(ActionHandler* h, int a)
	: Component(), handler(h), action(a) {}

ActionComponent::~ActionComponent() {}

int ActionComponent::getAction() const {
	return action;
}

ActionHandler* ActionComponent::getActionHandler() {
	return handler;
}

const ActionHandler* ActionComponent::getActionHandler() const {
	return handler;
}

void ActionComponent::setAction(int a) {
	action = a;
}

void ActionComponent::setActionHandler(ActionHandler* h) {
	handler = h;
}
