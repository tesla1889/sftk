/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <SFTK/Container.hpp>

using namespace sf;
using namespace sftk;

void Component::onBoundsChanged(float dl, float dt, float dw, float dh) {}

void Component::onDisabled() {}

void Component::onEnabled() {}

void Component::onEntered() {
	hover = true;
}

void Component::onFocusGained() {
	focus = true;
}

void Component::onFocusLost() {
	focus = false;
}

bool Component::onJoystickButtonPressed(unsigned id, unsigned button, float x, float y) {
	return false;
}

bool Component::onJoystickButtonReleased(unsigned id, unsigned button, float x, float y) {
	return false;
}

bool Component::onJoystickMoved(unsigned id, float x, float y) {
	return false;
}

bool Component::onKeyPressed(const Event::KeyEvent& event) {
	return false;
}

bool Component::onKeyReleased(const Event::KeyEvent& event) {
	return false;
}

void Component::onLeft() {
	hover = false;
}

bool Component::onMouseButtonPressed(Mouse::Button button, float x, float y) {
	requestFocus();
	return true;
}

bool Component::onMouseButtonReleased(Mouse::Button button, float x, float y) {
	return true;
}

bool Component::onMouseMoved(float x, float y) {
	return true;
}

bool Component::onMouseScrolled(bool vertical, float delta, float x, float y) {
	return false;
}

bool Component::onTextEntered(Uint32 unicode) {
	return false;
}

bool Component::onTouchBegan(unsigned finger, float x, float y) {
	requestFocus();
	return true;
}

bool Component::onTouchEnded(unsigned finger, float x, float y) {
	return true;
}

bool Component::onTouchMoved(unsigned finger, float x, float y) {
	return true;
}

void Component::onVisibilityChanged() {}

Component::Component() : NonCopyable(),
	background(220,220,220), foreground(0,0,0), bounds(), parent(NULL),
	margin(), enabled(true), focus(false), hover(false), visible(true) {}

Component::~Component() {
	if (parent != NULL)
		parent->removeComponent(this);
}

bool Component::contains(const Vector2f& point) const {
	return bounds.contains(point);
}

bool Component::contains(float x, float y) const {
	return bounds.contains(x,y);
}

void Component::disable() {
	enabled = false;
	onDisabled();
}

void Component::draw(const Graphics& graphics, float z) const {
	if (visible) {
		graphics.setColor(background);
		graphics.fillRect(bounds,z);
	}
}

void Component::enable() {
	enabled = true;
	onEnabled();
}

const Color& Component::getBackground() const {
	return background;
}

const FloatRect& Component::getBounds() const {
	return bounds;
}

const Color& Component::getForeground() const {
	return foreground;
}

float Component::getGlobalLeft() const {
	return (((parent == NULL) ?
		0 : parent->getGlobalLeft()) + bounds.left);
}

const Vector2f& Component::getGlobalPosition() const {
	return Vector2f(getGlobalLeft(),getGlobalTop());
}

float Component::getGlobalTop() const {
	return (((parent == NULL) ?
		0 : parent->getGlobalTop()) + bounds.top);
}

float Component::getHeight() const {
	return bounds.height;
}

float Component::getLeft() const {
	return bounds.left;
}

float Component::getMargin() const {
	return margin;
}

Container* Component::getParent() {
	return parent;
}

const Container* Component::getParent() const {
	return parent;
}

const Vector2f& Component::getPosition() const {
	return Vector2f(bounds.left,bounds.top);
}

const Vector2f& Component::getSize() const {
	return Vector2f(bounds.width,bounds.height);
}

float Component::getTop() const {
	return bounds.top;
}

float Component::getWidth() const {
	return bounds.width;
}

bool Component::hasFocus() const {
	return focus;
}

bool Component::isContainer() const {
	return false;
}

bool Component::isEnabled() const {
	return enabled;
}

bool Component::isHovered() const {
	return hover;
}

bool Component::isScrollbar() const {
	return false;
}

bool Component::isVisible() const {
	return visible;
}

void Component::requestFocus() {
	if (parent != NULL)
		parent->setFocus(this);
}

void Component::setBackground(const Color& b) {
	background.r = b.r;
	background.g = b.g;
	background.b = b.b;
	background.a = b.a;
}

void Component::setBackground(Uint8 r, Uint8 g, Uint8 b, Uint8 a) {
	background.r = r;
	background.g = g;
	background.b = b;
	background.a = a;
}

void Component::setBounds(const FloatRect& b) {
	setBounds(b.left,b.top,b.width,b.height);
}

void Component::setBounds(float left, float top, float width, float height) {
	float dl = (left - bounds.left);
	float dt = (top - bounds.top);
	float dw = (width - bounds.width);
	float dh = (height - bounds.height);

	bounds.left = left;
	bounds.top = top;
	bounds.width = width;
	bounds.height = height;

	onBoundsChanged(dl,dt,dw,dh);
}

void Component::setForeground(const Color& f) {
	foreground.r = f.r;
	foreground.g = f.g;
	foreground.b = f.b;
	foreground.a = f.a;
}

void Component::setForeground(Uint8 r, Uint8 g, Uint8 b, Uint8 a) {
	foreground.r = r;
	foreground.g = g;
	foreground.b = b;
	foreground.a = a;
}

void Component::setHeight(float height) {
	float dh = (height - bounds.height);

	bounds.height = height;

	onBoundsChanged(0,0,0,dh);
}

void Component::setLeft(float left) {
	float dl = (left - bounds.left);

	bounds.left = left;

	onBoundsChanged(dl,0,0,0);
}

void Component::setMargin(float m) {
	margin = m;
}

void Component::setParent(Container* p) {
	if (parent != NULL)
		parent->removeComponent(this);
	if ((parent = p) != NULL)
		parent->addComponent(this);
}

void Component::setPosition(const Vector2f& p) {
	setPosition(p.x,p.y);
}

void Component::setPosition(float left, float top) {
	float dl = (left - bounds.left);
	float dt = (top - bounds.top);

	bounds.left = left;
	bounds.top = top;

	onBoundsChanged(dl,dt,0,0);
}

void Component::setSize(const Vector2f& s) {
	setSize(s.x,s.y);
}

void Component::setSize(float width, float height) {
	float dw = (width - bounds.width);
	float dh = (height - bounds.height);

	bounds.width = width;
	bounds.height = height;

	onBoundsChanged(0,0,dw,dh);
}

void Component::setTop(float top) {
	float dt = (top - bounds.top);

	bounds.top = top;

	onBoundsChanged(0,dt,0,0);
}

void Component::setVisible(bool v) {
	if (visible != v) {
		visible = v;
		onVisibilityChanged();
	}
}

void Component::setWidth(float width) {
	float dw = (width - bounds.width);

	bounds.width = width;

	onBoundsChanged(0,0,dw,0);
}
