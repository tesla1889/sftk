#include <SFTK/Button.hpp>

using namespace sf;
using namespace sftk;

void Button::onBoundsChanged(float dl, float dt, float dw, float dh) {
	text.setPosition(bounds.left,bounds.top);
}

bool Button::onKeyPressed(const Event::KeyEvent& event) {
	if (isEnabled() && (event.code == Keyboard::Return)) {
		perform();
		return true;
	}

	return false;
}

bool Button::onMouseButtonPressed(Mouse::Button button, float x, float y) {
	if (isEnabled() && (button == Mouse::Button::Left)) {
		perform();
		return true;
	}

	return false;
}

bool Button::onTouchBegan(unsigned finger, float x, float y) {
	if (isEnabled()) {
		perform();
		return true;
	}

	return false;
}

Button::Button(const String& t, ActionHandler* h, int a)
	: ActionComponent(h,a), text(t) {}

Button::Button(const char* t, ActionHandler* h, int a)
	: ActionComponent(h,a), text(String(t)) {}

void Button::draw(const Graphics& graphics, float z) const {
	// TODO
}

const String& Button::getText() const {
	return text.getString();
}

void Button::setText(const String& t) {
	text.setString(t);
}

void Button::setText(const char* t) {
	text.setString(String(t));
}
