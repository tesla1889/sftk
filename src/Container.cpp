/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <algorithm>
#include <SFTK/Dropdown.hpp>

using namespace sf;
using namespace sftk;
using namespace std;

void Container::fixAll() {
	for (Iterator c = dropdowns.begin(); c != dropdowns.end(); ++c)
		fixChild(*c);
	for (Iterator c = children.begin(); c != children.end(); ++c)
		fixChild(*c);
}

void Container::fixChild(Component* child) {
	FloatRect b(child->getBounds());
	bool changed = false;

	if (b.left < 0) {
		b.left = 0;
		changed = true;
	}

	if (b.top < 0) {
		b.top = 0;
		changed = true;
	}

	float dx = ((b.left + b.width) - bounds.width);
	if (dx > 0) {
		if (b.left > dx)
			b.left -= dx;
		else {
			dx -= b.left;
			b.left = 0;
			b.width -= dx;
		}
		changed = true;
	}

	float dy = ((b.top - b.height) - bounds.height);
	if (dy > 0) {
		if (b.top > dy)
			b.top -= dy;
		else {
			dy -= b.top;
			b.top = 0;
			b.width -= dy;
		}
		changed = true;
	}

	if (changed)
		child->setBounds(b);
}

void Container::onBoundsChanged(float dl, float dt, float dw, float dh) {
	fixAll();
}

void Container::onFocusGained() {
	setFocus(NULL);
}

bool Container::onJoystickButtonPressed(unsigned id, unsigned button, float x, float y) {
	if (!isEnabled()) return false;

	x -= bounds.left;
	y -= bounds.top;

	for (Iterator c = dropdowns.begin(); c != dropdowns.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->onJoystickButtonPressed(id,button,x,y))
				return true;
		}
	}
	for (Iterator c = children.begin(); c != children.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->onJoystickButtonPressed(id,button,x,y))
				return true;
		}
	}

	return false;
}

bool Container::onJoystickButtonReleased(unsigned id, unsigned button, float x, float y) {
	if (!isEnabled()) return false;

	x -= bounds.left;
	y -= bounds.top;

	for (Iterator c = dropdowns.begin(); c != dropdowns.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->onJoystickButtonReleased(id,button,x,y))
				return true;
		}
	}
	for (Iterator c = children.begin(); c != children.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->onJoystickButtonReleased(id,button,x,y))
				return true;
		}
	}

	return false;
}

bool Container::onJoystickMoved(unsigned id, float x, float y) {
	if (!isEnabled()) return false;

	x -= bounds.left;
	y -= bounds.top;

	for (Iterator c = dropdowns.begin(); c != dropdowns.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->onJoystickMoved(id,x,y))
				return true;
		}
	}
	for (Iterator c = children.begin(); c != children.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (!child->isContainer())
				setHover(child);
			if (child->onJoystickMoved(id,x,y))
				return true;
		}
	}

	return false;
}

bool Container::onKeyPressed(const Event::KeyEvent& event, float x, float y) {
	if (!isEnabled()) return true;

	x -= bounds.left;
	y -= bounds.top;

	for (Iterator c = dropdowns.begin(); c != dropdowns.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (((Container*)child)->onKeyPressed(event,x,y))
				return true;
		}
	}
	for (Iterator c = children.begin(); c != children.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->isContainer()) {
				if (((Container*)child)->onKeyPressed(event,x,y))
					return true;
			} else {
				if (child->onKeyPressed(event))
					return true;
			}
		}
	}

	return false;
}

bool Container::onMouseButtonPressed(Mouse::Button button, float x, float y) {
	if (!isEnabled()) return false;

	x -= bounds.left;
	y -= bounds.top;

	for (Iterator c = dropdowns.begin(); c != dropdowns.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->onMouseButtonPressed(button,x,y))
				return true;
		}
	}
	for (Iterator c = children.begin(); c != children.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->onMouseButtonPressed(button,x,y))
				return true;
		}
	}

	return false;
}

bool Container::onMouseButtonReleased(Mouse::Button button, float x, float y) {
	if (!isEnabled()) return false;

	x -= bounds.left;
	y -= bounds.top;

	for (Iterator c = dropdowns.begin(); c != dropdowns.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->onMouseButtonReleased(button,x,y))
				return true;
		}
	}
	for (Iterator c = children.begin(); c != children.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->onMouseButtonReleased(button,x,y))
				return true;
		}
	}

	return false;
}

bool Container::onMouseMoved(float x, float y) {
	if (!isEnabled()) return false;

	x -= bounds.left;
	y -= bounds.top;

	for (Iterator c = dropdowns.begin(); c != dropdowns.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->onMouseMoved(x,y))
				return true;
		}
	}
	for (Iterator c = children.begin(); c != children.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (!child->isContainer())
				setHover(child);
			if (child->onMouseMoved(x,y))
				return true;
		}
	}

	return false;
}

bool Container::onMouseScrolled(bool vertical, float delta, float x, float y) {
	if (!isEnabled()) return false;

	x -= bounds.left;
	y -= bounds.top;

	for (Iterator c = dropdowns.begin(); c != dropdowns.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->onMouseScrolled(vertical,delta,x,y))
				return true;
		}
	}
	for (Iterator c = children.begin(); c != children.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (!child->isContainer())
				setHover(child);
			if (child->onMouseScrolled(vertical,delta,x,y))
				return true;
		}
	}

	return false;
}

bool Container::onTouchBegan(unsigned finger, float x, float y) {
	if (!isEnabled()) return false;

	x -= bounds.left;
	y -= bounds.top;

	for (Iterator c = dropdowns.begin(); c != dropdowns.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->onTouchBegan(finger,x,y))
				return true;
		}
	}
	for (Iterator c = children.begin(); c != children.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->onTouchBegan(finger,x,y))
				return true;
		}
	}

	return false;
}

bool Container::onTouchEnded(unsigned finger, float x, float y) {
	if (!isEnabled()) return false;

	x -= bounds.left;
	y -= bounds.top;

	for (Iterator c = dropdowns.begin(); c != dropdowns.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->onTouchEnded(finger,x,y))
				return true;
		}
	}
	for (Iterator c = children.begin(); c != children.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->onTouchEnded(finger,x,y))
				return true;
		}
	}

	return false;
}

bool Container::onTouchMoved(unsigned finger, float x, float y) {
	if (!isEnabled()) return false;

	x -= bounds.left;
	y -= bounds.top;

	for (Iterator c = dropdowns.begin(); c != dropdowns.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->onTouchMoved(finger,x,y))
				return true;
		}
	}
	for (Iterator c = children.begin(); c != children.end(); ++c) {
		Component* child = (*c);
		if (child->contains(x,y)) {
			if (child->onTouchMoved(finger,x,y))
				return true;
		}
	}

	return false;
}

Container::Container() : Component(),
	children(), dropdowns() {}

Container::~Container() {
	for (Iterator c = dropdowns.begin(); c != dropdowns.end(); ++c) {
		Component* child = (*c);
		child->parent = NULL;
		delete child;
	}
	for (Iterator c = children.begin(); c != children.end(); ++c) {
		Component* child = (*c);
		child->parent = NULL;
		delete child;
	}
}

void Container::addComponent(Component* component) {
	if (component->parent != NULL)
		component->parent->removeComponent(component);
	component->parent = this;
	if (component->isContainer()) {
		if (((Container*)component)->isDropdown())
			dropdowns.push_back(component);
		else
			children.push_back(component);
	}
}

void Container::draw(const Graphics& graphics, float z) const {
	if (isVisible()) {
		Component::draw(graphics,z);

		Graphics g(graphics);
		g.setBounds(bounds);

		for (ConstIterator c = children.begin(); c != children.end(); ++c)
			(*c)->draw(g,z);
		for (ConstIterator c = dropdowns.begin(); c != dropdowns.end(); ++c)
			(*c)->draw(g,z);
	}
}

size_t Container::getCount() const {
	return (dropdowns.size() + children.size());
}

Component* Container::getFocus() {
	return ((parent == NULL) ?
		NULL : parent->getFocus());
}

const Component* Container::getFocus() const {
	return ((parent == NULL) ?
		NULL : parent->getFocus());
}

Component* Container::getHover() {
	return ((parent == NULL) ?
		NULL : parent->getHover());
}

const Component* Container::getHover() const {
	return ((parent == NULL) ?
		NULL : parent->getHover());
}

bool Container::isContainer() const {
	return true;
}

bool Container::isDropdown() const {
	return false;
}

void Container::removeComponent(Component* component) {
	if (component->isContainer() && ((Container*)component)->isDropdown())
		dropdowns.erase(remove(dropdowns.begin(),dropdowns.end(),component),dropdowns.end());
	else
		children.erase(remove(children.begin(),children.end(),component),children.end());
}

void Container::setFocus(Component* component) {
	if (parent != NULL)
		parent->setFocus(component);
}

void Container::setHover(Component* component) {
	if (parent != NULL)
		parent->setHover(component);
}
