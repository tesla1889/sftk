/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <SFTK/Checkbox.hpp>

using namespace sf;
using namespace sftk;

void Checkbox::onJoystickButtonPressed(unsigned id, unsigned button, float x, float y) {
	if (isEnabled())
		setChecked(!checked);
}

bool Checkbox::onKeyPressed(const Event::KeyEvent& event) {
	if (isEnabled() && (event.code == Keyboard::Return)) {
		setChecked(!checked);
		return true;
	}

	return false;
}

bool Checkbox::onMouseButtonPressed(Mouse::Button button, float x, float y) {
	if (isEnabled() && (button == Mouse::Button::Left)) {
		setChecked(!checked);
		return true;
	}

	return false;
}

bool Checkbox::onTouchBegan(unsigned finger, float x, float y) {
	if (isEnabled()) {
		setChecked(!checked);
		return true;
	}

	return false;
}

Checkbox::Checkbox(bool c, ActionHandler* h, int a)
	: ActionComponent(h,a), checked(c) {}

Checkbox::~Checkbox() {}

void Checkbox::draw(const Graphics& graphics, float z) const {
	// TODO
}

bool Checkbox::isChecked() const {
	return checked;
}

void Checkbox::setChecked(bool c) {
	checked = c;
	perform();
}
