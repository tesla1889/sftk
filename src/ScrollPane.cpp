/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <iostream>
#include <SFTK/ScrollPane.hpp>

using namespace sf;
using namespace sftk;
using namespace std;

void ScrollPane::fixScrollbars() {
	float h = ((hscroll == NULL) ? 0 : hscroll->bounds.height);
	float w = ((vscroll == NULL) ? 0 : vscroll->bounds.width);

	if (hscroll != NULL) {
		hscroll->setBounds(
			0,(bounds.height - hscroll->bounds.height),
			(bounds.width - w),hscroll->bounds.height);
	}
	if (vscroll != NULL) {
		hscroll->setBounds(
			(bounds.width - vscroll->bounds.width),0,
			vscroll->bounds.width,(bounds.height - h));
	}
}

void ScrollPane::onBoundsChanged(float dl, float dt, float dw, float dh) {
	Container::onBoundsChanged(dl,dt,dw,dh);
	fixScrollbars();
}

bool ScrollPane::onJoystickButtonPressed(unsigned id, unsigned button, float x, float y) {
	if (!isEnabled()) return false;

	float x0 = (x - bounds.left);
	float y0 = (y - bounds.top);

	if ((hscroll != NULL) && hscroll->contains(x0,y0)) {
		if (hscroll->onJoystickButtonPressed(id,button,x0,y0))
			return true;
	} else if ((vscroll != NULL) && vscroll->contains(x0,y0)) {
		if (vscroll->onJoystickButtonPressed(id,button,x0,y0))
			return true;
	} else {
		if (hscroll != NULL)
			x += hscroll->getScroll();
		if (vscroll != NULL)
			y += vscroll->getScroll();

		if (Container::onJoystickButtonPressed(id,button,x,y))
			return true;
	}

	return false;
}

bool ScrollPane::onJoystickButtonReleased(unsigned id, unsigned button, float x, float y) {
	if (!isEnabled()) return false;

	float x0 = (x - bounds.left);
	float y0 = (y - bounds.top);

	if ((hscroll != NULL) && hscroll->contains(x0,y0)) {
		if (hscroll->onJoystickButtonReleased(id,button,x0,y0))
			return true;
	} else if ((vscroll != NULL) && vscroll->contains(x0,y0)) {
		if (vscroll->onJoystickButtonReleased(id,button,x0,y0))
			return true;
	} else {
		if (hscroll != NULL)
			x += hscroll->getScroll();
		if (vscroll != NULL)
			y += vscroll->getScroll();

		if (Container::onJoystickButtonReleased(id,button,x,y))
			return true;
	}

	return false;
}

bool ScrollPane::onJoystickMoved(unsigned id, float x, float y) {
	if (!isEnabled()) return false;

	float x0 = (x - bounds.left);
	float y0 = (y - bounds.top);

	if ((hscroll != NULL) && hscroll->contains(x0,y0)) {
		setHover(hscroll);
		if (hscroll->onJoystickMoved(id,x0,y0))
			return true;
	} else if ((vscroll != NULL) && vscroll->contains(x0,y0)) {
		setHover(vscroll);
		if (vscroll->onJoystickMoved(id,x0,y0))
			return true;
	} else {
		if (hscroll != NULL)
			x += hscroll->getScroll();
		if (vscroll != NULL)
			y += vscroll->getScroll();

		if (Container::onJoystickMoved(id,x,y))
			return true;
	}

	return false;
}

bool ScrollPane::onKeyPressed(const Event::KeyEvent& event, float x, float y) {
	if (!isEnabled()) return false;

	float x0 = (x - bounds.left);
	float y0 = (y - bounds.top);

	if ((hscroll != NULL) && hscroll->contains(x0,y0)) {
		if (hscroll->onKeyPressed(event))
			return true;
	} else if ((vscroll != NULL) && vscroll->contains(x0,y0)) {
		if (vscroll->onKeyPressed(event))
			return true;
	} else {
		if (hscroll != NULL)
			x += hscroll->getScroll();
		if (vscroll != NULL)
			y += vscroll->getScroll();

		if (Container::onKeyPressed(event,x,y))
			return true;
	}

	return false;
}

bool ScrollPane::onMouseButtonPressed(sf::Mouse::Button button, float x, float y) {
	if (!isEnabled()) return false;

	float x0 = (x - bounds.left);
	float y0 = (y - bounds.top);

	if ((hscroll != NULL) && hscroll->contains(x0,y0)) {
		if (hscroll->onMouseButtonPressed(button,x0,y0))
			return true;
	} else if ((vscroll != NULL) && vscroll->contains(x0,y0)) {
		if (vscroll->onMouseButtonPressed(button,x0,y0))
			return true;
	} else {
		if (hscroll != NULL)
			x += hscroll->getScroll();
		if (vscroll != NULL)
			y += vscroll->getScroll();

		if (Container::onMouseButtonPressed(button,x,y))
			return true;
	}

	return false;
}

bool ScrollPane::onMouseButtonReleased(sf::Mouse::Button button, float x, float y) {
	if (!isEnabled()) return false;

	float x0 = (x - bounds.left);
	float y0 = (y - bounds.top);

	if ((hscroll != NULL) && hscroll->contains(x0,y0)) {
		if (hscroll->onMouseButtonReleased(button,x0,y0))
			return true;
	} else if ((vscroll != NULL) && vscroll->contains(x0,y0)) {
		if (vscroll->onMouseButtonReleased(button,x0,y0))
			return true;
	} else {
		if (hscroll != NULL)
			x += hscroll->getScroll();
		if (vscroll != NULL)
			y += vscroll->getScroll();

		if (Container::onMouseButtonReleased(button,x,y))
			return true;
	}

	return false;
}

bool ScrollPane::onMouseMoved(float x, float y) {
	if (!isEnabled()) return false;

	float x0 = (x - bounds.left);
	float y0 = (y - bounds.top);

	if ((hscroll != NULL) && hscroll->contains(x0,y0)) {
		setHover(hscroll);
		if (hscroll->onMouseMoved(x0,y0))
			return true;
	} else if ((vscroll != NULL) && vscroll->contains(x0,y0)) {
		setHover(vscroll);
		if (vscroll->onMouseMoved(x0,y0))
			return true;
	} else {
		if (hscroll != NULL)
			x += hscroll->getScroll();
		if (vscroll != NULL)
			y += vscroll->getScroll();

		if (Container::onMouseMoved(x,y))
			return true;
	}

	return false;
}

bool ScrollPane::onMouseScrolled(bool vertical, float delta, float x, float y) {
	if (!isEnabled()) return false;

	float x0 = (x - bounds.left);
	float y0 = (y - bounds.top);

	if ((hscroll != NULL) && hscroll->contains(x0,y0)) {
		if (hscroll->onMouseScrolled(vertical,delta,x0,y0))
			return true;
	} else if ((vscroll != NULL) && vscroll->contains(x0,y0)) {
		if (vscroll->onMouseScrolled(vertical,delta,x0,y0))
			return true;
	} else {
		if (hscroll != NULL)
			x += hscroll->getScroll();
		if (vscroll != NULL)
			y += vscroll->getScroll();

		if (Container::onMouseScrolled(vertical,delta,x,y))
			return true;
	}

	return false;
}

bool ScrollPane::onTouchBegan(unsigned finger, float x, float y) {
	if (!isEnabled()) return false;

	float x0 = (x - bounds.left);
	float y0 = (y - bounds.top);

	if ((hscroll != NULL) && hscroll->contains(x0,y0)) {
		if (hscroll->onTouchBegan(finger,x0,y0))
			return true;
	} else if ((vscroll != NULL) && vscroll->contains(x0,y0)) {
		if (vscroll->onTouchBegan(finger,x0,y0))
			return true;
	} else {
		if (hscroll != NULL)
			x += hscroll->getScroll();
		if (vscroll != NULL)
			y += vscroll->getScroll();

		if (Container::onTouchBegan(finger,x,y))
			return true;
	}

	return false;
}

bool ScrollPane::onTouchEnded(unsigned finger, float x, float y) {
	if (!isEnabled()) return false;

	float x0 = (x - bounds.left);
	float y0 = (y - bounds.top);

	if ((hscroll != NULL) && hscroll->contains(x0,y0)) {
		if (hscroll->onTouchEnded(finger,x0,y0))
			return true;
	} else if ((vscroll != NULL) && vscroll->contains(x0,y0)) {
		if (vscroll->onTouchEnded(finger,x0,y0))
			return true;
	} else {
		if (hscroll != NULL)
			x += hscroll->getScroll();
		if (vscroll != NULL)
			y += vscroll->getScroll();

		if (Container::onTouchEnded(finger,x,y))
			return true;
	}

	return false;
}

bool ScrollPane::onTouchMoved(unsigned finger, float x, float y) {
	if (!isEnabled()) return false;

	float x0 = (x - bounds.left);
	float y0 = (y - bounds.top);

	if ((hscroll != NULL) && hscroll->contains(x0,y0)) {
		if (hscroll->onTouchMoved(finger,x0,y0))
			return true;
	} else if ((vscroll != NULL) && vscroll->contains(x0,y0)) {
		if (vscroll->onTouchMoved(finger,x0,y0))
			return true;
	} else {
		if (hscroll != NULL)
			x += hscroll->getScroll();
		if (vscroll != NULL)
			y += vscroll->getScroll();

		if (Container::onTouchMoved(finger,x,y))
			return true;
	}

	return false;
}

ScrollPane::ScrollPane(Scrollbar* v, Scrollbar* h)
: Container(), vscroll(v), hscroll(h) {
	if (hscroll != NULL) {
		if (hscroll->parent != NULL)
			hscroll->setParent(NULL);
		hscroll->parent = this;
	}
	if (vscroll != NULL) {
		if (vscroll->parent != NULL)
			vscroll->setParent(NULL);
		vscroll->parent = this;
	}
}

ScrollPane::~ScrollPane() {
	if (hscroll != NULL) {
		hscroll->parent = NULL;
		delete hscroll;
	}
	if (vscroll != NULL) {
		vscroll->parent = NULL;
		delete vscroll;
	}
}

void ScrollPane::addComponent(Component* component) {
	if (component->isScrollbar())
		cerr		<< "SFTK: use ScrollPane::setHorizontalScrollbar or "
				<< "ScrollPane::setVerticalScrollbar to add a "
				<< "Scrollbar to a ScrollPane." << endl;
	else
		Container::addComponent(component);
}

void ScrollPane::draw(const Graphics& graphics, float z = 0) const {
	// TODO
}

Scrollbar* ScrollPane::getHorizontalScrollbar() {
	return hscroll;
}

const Scrollbar* ScrollPane::getHorizontalScrollbar() const {
	return hscroll;
}

Scrollbar* ScrollPane::getVerticalScrollbar() {
	return vscroll;
}

const Scrollbar* ScrollPane::getVerticalScrollbar() const {
	return vscroll;
}

void ScrollPane::removeComponent(Component* component) {
	if (component->isScrollbar())
		cerr		<< "SFTK: use ScrollPane::setHorizontalScrollbar or "
				<< "ScrollPane::setVerticalScrollbar to remove a "
				<< "Scrollbar to a ScrollPane." << endl;
	else
		Container::removeComponent(component);
}

void ScrollPane::setHorizontalScrollbar(Scrollbar* h) {
	if (hscroll != NULL) {
		hscroll->parent = NULL;
		delete hscroll;
	}

	(hscroll = h)->parent = this;
	fixScrollbars();
}

void ScrollPane::setVerticalScrollbar(Scrollbar* v) {
	if (vscroll != NULL) {
		vscroll->parent = NULL;
		delete vscroll;
	}

	(vscroll = v)->parent = this;
	fixScrollbars();
}
