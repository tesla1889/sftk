#ifndef SFTK_COMPONENT_HPP
#define SFTK_COMPONENT_HPP

/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "Graphics.hpp"

namespace sftk {

class Container;

class Component : public sf::NonCopyable {
protected:
	sf::Color background;
	sf::Color foreground;
	sf::FloatRect bounds;
	Container* parent;
	float margin;
private:
	bool enabled, focus, hover, visible;
protected:
	virtual void onBoundsChanged(float dl, float dt, float dw, float dh);
	virtual void onDisabled();
	virtual void onEnabled();
	virtual void onEntered();
	virtual void onFocusGained();
	virtual void onFocusLost();
	virtual bool onJoystickButtonPressed(unsigned id, unsigned button, float x, float y);
	virtual bool onJoystickButtonReleased(unsigned id, unsigned button, float x, float y);
	virtual bool onJoystickMoved(unsigned id, float x, float y);
	virtual bool onKeyPressed(const sf::Event::KeyEvent& event);
	virtual bool onKeyReleased(const sf::Event::KeyEvent& event);
	virtual void onLeft();
	virtual bool onMouseButtonPressed(sf::Mouse::Button button, float x, float y);
	virtual bool onMouseButtonReleased(sf::Mouse::Button button, float x, float y);
	virtual bool onMouseMoved(float x, float y);
	virtual bool onMouseScrolled(bool vertical, float delta, float x, float y);
	virtual bool onTextEntered(sf::Uint32 unicode);
	virtual bool onTouchBegan(unsigned finger, float x, float y);
	virtual bool onTouchEnded(unsigned finger, float x, float y);
	virtual bool onTouchMoved(unsigned finger, float x, float y);
	virtual void onVisibilityChanged();
public:
	Component();
	virtual ~Component();

	bool contains(const sf::Vector2f& point) const;
	virtual bool contains(float x, float y) const;
	void disable();
	virtual void draw(const Graphics& graphics, float z = 0) const;
	void enable();
	const sf::Color& getBackground() const;
	const sf::FloatRect& getBounds() const;
	const sf::Color& getForeground() const;
	float getGlobalLeft() const;
	const sf::Vector2f& getGlobalPosition() const;
	float getGlobalTop() const;
	float getHeight() const;
	float getLeft() const;
	float getMargin() const;
	Container* getParent();
	const Container* getParent() const;
	const sf::Vector2f& getPosition() const;
	const sf::Vector2f& getSize() const;
	float getTop() const;
	float getWidth() const;
	bool hasFocus() const;
	virtual bool isContainer() const;
	bool isEnabled() const;
	bool isHovered() const;
	virtual bool isScrollbar() const;
	bool isVisible() const;
	void requestFocus();
	void setBackground(const sf::Color& background);
	void setBackground(
		const sf::Uint8 r, const sf::Uint8 g,const sf::Uint8 b, const sf::Uint8 a = 255);
	void setBounds(const sf::FloatRect& bounds);
	void setBounds(float left, float top, float width, float height);
	void setForeground(const sf::Color& foreground);
	void setForeground(
		const sf::Uint8 r, const sf::Uint8 g,const sf::Uint8 b, const sf::Uint8 a = 255);
	void setHeight(float height);
	void setLeft(float left);
	void setMargin(float margin);
	void setParent(Container* parent);
	void setPosition(const sf::Vector2f& position);
	void setPosition(float left, float top);
	void setSize(const sf::Vector2f& size);
	void setSize(float width, float height);
	void setTop(float top);
	void setVisible(bool visible = true);
	void setWidth(float width);
};

}

#endif
