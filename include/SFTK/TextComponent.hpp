#ifndef SFTK_TEXTCOMPONENT_HPP
#define SFTK_TEXTCOMPONENT_HPP

/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <string>
#include "Component.hpp"

namespace sftk {

class TextComponent : public Component {
protected:
	enum Mode {
		TEXTMODE_NONE = 0,
		TEXTMODE_INSERT = 1,
		TEXTMODE_SELECT = 2,
	};

	sf::Color selectionBackground;
	sf::Color selectionForeground;
	std::string content;
	size_t cursor;
	ssize_t prefixEnd, suffixStart;
	int mode;

	virtual bool onKeyPressed(const sf::Event::KeyEvent& event);
	virtual bool onTextEntered(sf::Uint32 unicode);
public:
	TextComponent(const char* text = NULL);
	TextComponent(const std::string& text);
	virtual ~TextComponent();

	size_t getCursor() const;
	size_t getLength() const;
	const std::string& getSelectedText() const;
	const sf::Color& getSelectionBackground() const;
	const sf::Color& getSelectionForeground() const;
	const std::string& getText() const;
	void setCursor(size_t cursor);
	void setSelectedText(const char* text);
	void setSelectedText(const std::string& text);
	void setSelectionBackground(const sf::Color& background);
	void setSelectionBackground(sf::Uint8 r, sf::Uint8 g, sf::Uint8 b, sf::Uint8 a = 255);
	void setSelectionForeground(const sf::Color& foreground);
	void setSelectionForeground(sf::Uint8 r, sf::Uint8 g, sf::Uint8 b, sf::Uint8 a = 255);
	void setText(const char* text);
	void setText(const std::string& text);
};

}

#endif
