#ifndef SFTK_SLIDER_HPP
#define SFTK_SLIDER_HPP

/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "ActionComponent.hpp"

namespace sftk {

class Slider : public ActionComponent {
protected:
	float step;
	float value;
	bool vertical;

	virtual void onCursorSelect(float x, float y);
	virtual bool onKeyPressed(const sf::Event::KeyEvent& event);
	virtual bool onMouseButtonPressed(sf::Mouse::Button button, float x, float y);
	virtual bool onMouseMoved(float x, float y);
	virtual bool onMouseScrolled(bool vertical, float delta, float x, float y);
	virtual bool onTouchBegan(unsigned finger, float x, float y);
	virtual bool onTouchMoved(unsigned finger, float x, float y);
public:
	Slider(bool vertical = false, float value = 0.5f, float step = 0.05f);
	virtual ~Slider();

	virtual void decrement();
	virtual void draw(const Graphics& graphics, float z = 0) const;
	virtual void increment();
	virtual bool isVertical() const;
	float getStep() const;
	float getValue() const;
	void setStep(float step);
	void setValue(float value);
	void setVertical(bool vertical = true);
};

}

#endif
