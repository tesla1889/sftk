#ifndef SFTK_FRAME_HPP
#define SFTK_FRAME_HPP

/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <thread>
#include "Container.hpp"

namespace sftk {

class Frame {
private:
	std::thread _thread;
protected:
	Container* content;
	sf::RenderWindow* window;
	bool started;

	virtual void onBoundsChanged(float dl, float dt, float dw, float dh);
	virtual bool onJoystickButtonPressed(unsigned id, unsigned button, float x, float y);
	virtual bool onJoystickButtonReleased(unsigned id, unsigned button, float x, float y);
	virtual bool onJoystickMoved(unsigned id, float x, float y);
	virtual bool onKeyPressed(const sf::Event::KeyEvent& event);
	virtual bool onKeyReleased(const sf::Event::KeyEvent& event);
	virtual bool onMouseButtonPressed(sf::Mouse::Button button, float x, float y);
	virtual bool onMouseButtonReleased(sf::Mouse::Button button, float x, float y);
	virtual bool onMouseMoved(float x, float y);
	virtual bool onMouseScrolled(bool vertical, float delta, float x, float y);
	virtual bool onTextEntered(sf::Uint32 unicode);
	virtual bool onTouchBegan(unsigned finger, float x, float y);
	virtual bool onTouchEnded(unsigned finger, float x, float y);
	virtual bool onTouchMoved(unsigned finger, float x, float y);
	virtual void onVisibilityChanged();
public:
	Frame(sf::RenderWindow* window, Container* content = NULL);
	virtual ~Frame();

	virtual void draw(const Graphics& graphics, float z = 0) const;
	void join();

	friend void frame_run(Frame* frame);
};

void frame_run(Frame* frame);

}

#endif
