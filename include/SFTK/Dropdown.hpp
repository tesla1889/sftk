#ifndef SFTK_DROPDOWN_HPP
#define SFTK_DROPDOWN_HPP

/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "Container.hpp"

namespace sftk {

class Dropdown : public Container {
protected:
	sf::Vector2f downSize;
	bool downShow;

	virtual void fixChild(Component* child);
	void getDownSize();
	virtual void onBoundsChanged(float dl, float dt, float dw, float dh);
	virtual bool onJoystickButtonPressed(unsigned id, unsigned button, float x, float y);
	virtual bool onJoystickButtonReleased(unsigned id, unsigned button, float x, float y);
	virtual bool onJoystickMoved(unsigned id, float x, float y);
	virtual bool onMouseButtonPressed(sf::Mouse::Button button, float x, float y);
	virtual bool onMouseButtonReleased(sf::Mouse::Button button, float x, float y);
	virtual bool onMouseMoved(float x, float y);
	virtual bool onTouchBegan(unsigned finger, float x, float y);
	virtual bool onTouchEnded(unsigned finger, float x, float y);
	virtual bool onTouchMoved(unsigned finger, float x, float y);
	virtual void onVisibilityChanged();
public:
	Dropdown();
	virtual ~Dropdown();

	virtual void addComponent(Component* component);
	virtual bool contains(float x, float y) const;
	virtual void draw(const Graphics& graphics, float z = 0) const = 0;
	virtual size_t getCount() const;
	virtual void removeComponent(Component* component);
};

}

#endif
