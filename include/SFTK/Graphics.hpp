#ifndef SFTK_GRAPHICS_HPP
#define SFTK_GRAPHICS_HPP

/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <mutex>
#include <SFML/Graphics.hpp>

namespace sftk {

class Graphics {
private:
	static std::mutex _mutex;

	sf::FloatRect bounds;
	sf::RenderWindow* target;

	void fix(float& left, float& top, float& width, float& height) const;
public:
	Graphics(const Graphics& graphics);
	Graphics(const sf::RenderWindow* target);

	void changeBounds(float dl, float dt, float dw, float dh);
	void drawRect(const sf::FloatRect& rect, float z = 0) const;
	void drawRect(float left, float top, float width, float height, float z = 0) const;
	void drawString(const sf::String& s, float left, float top) const;
	void fillRect(const sf::FloatRect& rect, float z = 0) const;
	void fillRect(float left, float top, float width, float height, float z = 0) const;
	const sf::FloatRect& getBounds() const;
	const sf::RenderWindow* getTarget() const;
	void setBounds(const sf::FloatRect& bounds);
	void setBounds(float left, float top, float width, float height);
	void setColor(const sf::Color& color) const;
	void setColor(const sf::Uint8 r, const sf::Uint8 g,
		const sf::Uint8 b, const sf::Uint8 a = 255) const;
};

}

#endif
