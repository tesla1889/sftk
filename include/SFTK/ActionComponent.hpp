#ifndef SFTK_ACTIONCOMPONENT_HPP
#define SFTK_ACTIONCOMPONENT_HPP

/*
 * SFTK - Simple and Fast Toolkit
 * Copyright (C) 2017 Nick Trebes (nicktrebes@tamu.edu)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment
 * in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "Component.hpp"

namespace sftk {

class ActionComponent;

struct ActionHandler {
	virtual ~ActionHandler();

	virtual void onAction(int action, ActionComponent* source) = 0;
};

class ActionComponent : public Component {
protected:
	ActionHandler* handler;
	int action;

	virtual void perform();
public:
	ActionComponent(ActionHandler* handler = NULL, int action = 0);
	virtual ~ActionComponent();

	int getAction() const;
	ActionHandler* getActionHandler();
	const ActionHandler* getActionHandler() const;
	void setAction(int action);
	void setActionHandler(ActionHandler* handler);
};

}

#endif
